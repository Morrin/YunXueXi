import axios from 'axios';

let base = '';

let instance = axios.create({
  headers: {
    'Content-Type': 'application/json'
  }
});

/*
* 用户登陆
* */
export const requestLogin = params => { return instance.post(`/api/user/login`, params).then(res => res.data); };

/*
* 院系管理(department)
* */
export const getDepartmentList = params => { return instance.get(`/api/department/list/name?count=`+params.count+`&pageNo=`+params.pageNo+`&name=`+params.name+`&sortDirection=`+params.sortDirection+`&sortField=`+params.sortField); };
export const removeDepartmentData = params => { return instance.get(`/api/department/delete`, params); };
export const saveDepartmentData = params => { return instance.post(`/api/department/save`, params); };

/*
* 专业管理(specialty)
* */
export const getSpecialtyList = params => { return instance.get(`/api/specialty/list/search?count=`+params.count+`&pageNo=`+params.pageNo+`&name=`+params.name+`&departmentId=`+params.departmentId+`&sortDirection=`+params.sortDirection+`&sortField=`+params.sortField); };
export const removeSpecialtyData = params => { return instance.get(`/api/specialty/delete`, params); };
export const saveSpecialtyData = params => { return instance.post(`/api/specialty/save`, params); };

/*
* 班级管理(class)
* */
export const getClassList = params => { return instance.get(`/api/class/list?count=`+params.count+`&pageNo=`+params.pageNo+`&name=`+params.name+`&year=`+params.year+`&departmentId=`+params.departmentId+`&specialtyId=`+params.specialtyId+`&sortDirection=`+params.sortDirection+`&sortField=`+params.sortField); };
export const removeClassData = params => { return instance.get(`/api/class/delete`, params); };
export const saveClassData = params => { return instance.post(`/api/class/save`, params); };

/*
* 课程管理(course)
* */
export const searchCourseData = params => { return instance.get(`/api/course/getById?id=`+params ); };
export const getCourseList = params => { return instance.get(`/api/course/list?count=`+params.count+`&pageNo=`+params.pageNo+`&name=`+params.name+`&departmentId=`+params.departmentId+`&specialtyId=`+params.specialtyId+`&sortDirection=`+params.sortDirection+`&sortField=`+params.sortField); };
export const removeCourseData = params => { return instance.get(`/api/course/delete`, params); };
export const saveCourseData = params => { return instance.post(`/api/course/save`, params); };

/*
* 课程关联管理(relevancy)
* */
export const getRelevancyList = params => { return instance.post(`/api/specialtyCourse/list`, params); };
export const removeRelevancyData = params => { return instance.post(`/api/specialtyCourse/delete`, params); };
export const saveRelevancyData = params => { return instance.post(`/api/specialtyCourse/save`, params); };

/*
* 用户管理(user)
* */
export const searchUserData = params => { return instance.get(`/api/user/getById?id=`+params ); };
export const getTeacherList = params => { return instance.post(`/api/user/list/teacher`, params); };
export const getStudentList = params => { return instance.post(`/api/user/list/student`, params); };
export const getAdminList = params => { return instance.post(`/api/user/list/admin`, params); };
export const removeUserData = params => { return instance.get(`/api/user/delete`, params); };
export const upDateUserData = params => { return instance.post(`/api/user/update`, params); };
export const saveUserData = params => { return instance.post(`/api/user/save`, params); };

/*
* 上传文件(fileUpload)
* */
export const uploadRecordUpload = params => { return instance.post(`/api/file/user/record/upload`, params); };

