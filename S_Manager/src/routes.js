import Login from './views/Login.vue'
import NotFound from './views/404.vue'
import Home from './views/Home.vue'
import Main from './views/Main.vue'
import Department from './views/school/Department.vue'
import Specialty from './views/school/Specialty.vue'
import Class from './views/school/Class.vue'
import Administrator from './views/users/Administrator.vue'
import Teacher from './views/users/Teacher.vue'
import Student from './views/users/Student.vue'
import Course from './views/course/Course.vue'
import Relevancy from './views/course/Relevancy.vue'

let routes = [
    {
        path: '/login',
        component: Login,
        name: '',
        hidden: true
    },
    {
        path: '/404',
        component: NotFound,
        name: '',
        hidden: true
    },
    //{ path: '/main', component: Main },
    {
        path: '/',
        component: Home,
        name: '',
        iconCls: 'fa fa-home',
        leaf: true,//只有一个节点
        children: [
            { path: '/main', component: Main, name: '主页' }
        ]
    },
    {
        path: '/',
        component: Home,
        name: '校园管理',
        iconCls: 'fa fa-envelope',//图标样式class
        children: [
            { path: '/department', component: Department, name: '院系信息管理' },
            { path: '/specialty', component: Specialty, name: '专业信息管理' },
            { path: '/class', component: Class, name: '班级信息管理' },
        ]
    },
    {
        path: '/',
        component: Home,
        name: '课程管理',
        iconCls: 'fa fa-id-card-o',
        children: [
            { path: '/course', component: Course, name: '课程信息管理' },
            { path: '/relevancy', component: Relevancy, name: '课程关联管理' }
        ]
    },
    {
        path: '/',
        component: Home,
        name: '用户管理',
        iconCls: 'fa fa-users',
        children: [
            { path: '/administrator', component: Administrator, name: '管理员信息管理' },
            { path: '/teacher', component: Teacher, name: '教师信息管理' },
            { path: '/student', component: Student, name: '学生信息管理' },
        ]
    },
    {
        path: '*',
        hidden: true,
        redirect: { path: '/404' }
    }
];

export default routes;