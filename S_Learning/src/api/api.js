import axios from 'axios';

let base = '';

var instance = axios.create({
  headers: {
    'Content-Type': 'application/json'
  }
});

/*
* 用户登陆
* */
export const requestLogin = params => { return instance.post(`/api/user/login`, params).then(res => res.data); };
export const searchUserData = params => { return instance.get(`/api/user/getById?id=`+params ); };
export const upDateUserData = params => { return instance.post(`/api/user/update`, params); };

/*
* 课程管理(class)
* */
export const getRelevancyList = params => { return instance.post(`/api/specialtyCourse/list`, params); };
export const searchCourseData = params => { return instance.get(`/api/course/getById?id=`+params ); };
export const getStudentAttendanceList = params => { return instance.post(`/api/studentAttendance/list`, params); };
export const getStudentScoreList = params => { return instance.post(`/api/studentScore/list`, params); };


/*
* 章节管理(class)
* */
export const getSectionStructure = params => { return instance.get(`/api/section/sectionStructure?courseId=`+ params.courseId+`&teacherId=`+params.teacherId); };
export const saveSectionData = params => { return instance.post(`/api/section/save`, params); };

/*
* 文章管理(class)
* */
export const searchArticleData = params => { return instance.get(`/api/article/getById?id=`+params); };
export const getArticleList = params => { return instance.post(`/api/article/list`, params); };
export const saveArticleData = params => { return instance.post(`/api/article/save`, params); };

/*
* 文章管理(class)
* */
export const getSearchBook = params => { return instance.post(`/api/library/list`, params); };