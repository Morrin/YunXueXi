import Mock from 'mockjs';

const Department = [];

for (let i = 0; i < 86; i++) {
  Department.push(Mock.mock({
    name: Mock.Random.cname()+'学院',
    remark: Mock.mock('@paragraph()'),
  }));
}

export { Department };
