import Login from './views/Login.vue'
import NotFound from './views/404.vue'
import Home from './views/Home.vue'
/*import Score from './views/student/Score.vue'
import CheckingIn from './views/student/CheckingIn.vue'
import Management from './views/student/Management.vue'
import Monitor from './views/users/Monitor.vue'*/
import SInfo from './views/users/SInfo.vue'
import Article from './views/course/Article.vue'
import ADetail from './views/course/ADetail.vue'
import CDetail from './views/course/CDetail.vue'
import Catalog from './views/course/Catalog.vue'
import Search from './views/library/Search.vue'

let routes = [
  {
    path: '/login',
    component: Login,
    name: '',
    hidden: true
  },
  {
    path: '/404',
    component: NotFound,
    name: '',
    hidden: true
  },
  {
    path: '/',
    component: Home,
    name: '用户个人',
    iconCls: 'fa fa-user',
    children: [
      // { path: '/monitor', component: Monitor, name: '课程监控' },
      { path: '/sinfo', component: SInfo, name: '个人信息' }
    ]
  },
  {
    path: '/',
    component: Home,
    name: '课程管理',
    iconCls: 'fa fa-envelope',//图标样式class
    children: [
      { path: '/catalog', component: Catalog, name: '课程目录'},
      { path: '/cdetail', component: CDetail, name: '课程详情', hidden: true},
      // { path: '/editor', component: Editor, name: '发布文章' },
      { path: '/article', component: Article, name: '文章目录', hidden: true },
      { path: '/adetail', component: ADetail, name: '文章详情', hidden: true}
    ]
  },
  {
    path: '/',
    component: Home,
    name: '图书馆',
    iconCls: 'fa fa-envelope',//图标样式class
    children: [
      { path: '/library', component: Search, name: '图书搜索'},
    ]
  },
  /*{
    path: '/',
    component: Home,
    name: '学生信息',
    iconCls: 'fa fa-graduation-cap',
    children: [
      { path: '/score', component: Score, name: '学生平时成绩' },
      { path: '/checkingin', component: CheckingIn, name: '学生考勤信息' },
      { path: '/management', component: Management, name: '学生信息管理' },
    ]
  },*/
  {
    path: '*',
    hidden: true,
    redirect: { path: '/404' }
  }
];

export default routes;